package org.com.restcontroller;

import java.util.List;

import org.com.entities.Book;
import org.com.entities.Users;
import org.com.exception.UserNotFoundException;
import org.com.service.LibraryService;
import org.com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/library")
@CrossOrigin("http://localhost:4200")
@Api(description = "This is simple UserService we can use this api to add,read,update and delete user" )
@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added new book"),
		@ApiResponse(code = 401, message = "Unauthorized", response = UserNotFoundException.class),
		@ApiResponse(code = 403, message = "Forbidden", response = UserNotFoundException.class),
		@ApiResponse(code = 404, message = "The resource you are trying is not found", response = UserNotFoundException.class),
		@ApiResponse(code = 500, message = "Internal Sever error", response = UserNotFoundException.class) })
public class UserRestController {
	@Autowired
	UserService userService;
	@Autowired
	LibraryService libraryService;
	@GetMapping("users")
	public ResponseEntity<List<Users>> getAllUsers() {
		return userService.getAllUsers();
	}

	@GetMapping("user/{user_id}")
	public ResponseEntity<Users> getuserById(@PathVariable long user_id) {
		return userService.getUserById(user_id);

	}
	@GetMapping("users/{user_id}")
	public ResponseEntity<List<Book>>getBooksCorrespondingToUser(@PathVariable long user_id){
		System.out.println("In user"+libraryService.booksCorrespondingToUser(user_id));
		return new ResponseEntity(libraryService.booksCorrespondingToUser(user_id),HttpStatus.OK);
		
	}

	@PostMapping(value = "users")
	public ResponseEntity<Users> addUser(@RequestBody Users user) {
		userService.addUser(user);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@DeleteMapping("users/{userId}")
	public ResponseEntity<Users> deleteUser(@PathVariable long userId) {
		userService.deleteUser(userId);
		libraryService.deleteBooksCorrespondingToUser(userId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping("users/{user_id}")
	public ResponseEntity<Users> updateUser(@PathVariable long user_id, @RequestBody Users user) {
		userService	.updateUser(user_id, user);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

}
