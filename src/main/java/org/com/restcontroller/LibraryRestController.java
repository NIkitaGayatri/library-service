package org.com.restcontroller;

import java.util.Objects;

import org.com.entities.Library;
import org.com.exception.BookNotFoundException;
import org.com.exception.InvalidInputException;
import org.com.exception.UserNotFoundException;
import org.com.service.BookService;
import org.com.service.LibraryService;
import org.com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/library")
@CrossOrigin("http://localhost:4200")
@Api(description = "This is simple library we can use this api to add,read,update and delete user,book" )
@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added new book"),
		@ApiResponse(code = 401, message = "Unauthorized", response = InvalidInputException.class),
		@ApiResponse(code = 403, message = "Forbidden", response = UserNotFoundException.class),
		@ApiResponse(code = 404, message = "The resource you are trying is not found", response = InvalidInputException.class),
		@ApiResponse(code = 500, message = "Internal Sever error", response = UserNotFoundException.class) })

public class LibraryRestController {
	@Autowired
	BookService bookService;
	@Autowired
	UserService userService;
	@Autowired
	LibraryService libraryService;

	@PostMapping("users/{user_id}/books/{book_id}")
	public ResponseEntity<Library> issueBookTOUser(@PathVariable("user_id") long userId,@PathVariable("book_id") int bookId) {
		if (Objects.isNull(userId)||Objects.isNull(bookId)) {
	        throw new InvalidInputException("User ID or Book ID can not be null!");
	    }
		checkIfUserForUserIdExists(userId);
		checkIfBookForBookIdExists(bookId);
		libraryService.issueBookToUser(userId, bookId);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@DeleteMapping("users/{user_id}/books/{book_id}")
	public ResponseEntity<Library> deleteEntryInLibrary(@PathVariable("user_id") long userId,@PathVariable("book_id") int bookId) {
		libraryService.deleteEntryInLibrary(userId, bookId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	private void checkIfBookForBookIdExists(int bookId){
		if (!(bookService.getBookById(bookId).getStatusCode().equals(HttpStatus.OK))) {
			throw new BookNotFoundException("Book with book Id " + bookId + " Not Found ");
		}

	}

	private void checkIfUserForUserIdExists(Long userId) {
		if (!(userService.getUserById(userId).getStatusCode().equals(HttpStatus.OK))) {
			throw new UserNotFoundException("User with user Id " + userId + " Not Found ");
		}
	}

}
