package org.com.restcontroller;

import java.util.List;

import org.com.entities.Book;
import org.com.exception.BookNotFoundException;
import org.com.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/library")
@CrossOrigin("http://localhost:4200")
@Api(description = "This is simple bookService we can use this api to add,read,update and delete books" )
@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added new book"),
		@ApiResponse(code = 401, message = "Unauthorized", response = BookNotFoundException.class),
		@ApiResponse(code = 403, message = "Forbidden", response = BookNotFoundException.class),
		@ApiResponse(code = 404, message = "The resource you are trying is not found", response = BookNotFoundException.class),
		@ApiResponse(code = 500, message = "Internal Sever error", response = BookNotFoundException.class) })
public class BookRestController {
	@Autowired
	BookService bookService;
	@GetMapping("/books")
	public ResponseEntity<List<Book>> getAllBooks() {
		return bookService.getAllBooks();
	}

	@GetMapping("/book/{id}")
	public ResponseEntity<Book> getBookById(@PathVariable int id) {
		return bookService.getBookById(id);
	}

	@PostMapping(value = "/book")
	public ResponseEntity<Book> addBook(@RequestBody Book book) {
		bookService.createBook(book);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@DeleteMapping("/book/{id}")
	public ResponseEntity<?> deleteBook(@PathVariable int id) {
		bookService.deleteBook(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping("/book/{id}")
	public ResponseEntity<Book> updateBook(@PathVariable int id, @RequestBody Book book) {
		bookService.updateBook(id, book);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}



}
