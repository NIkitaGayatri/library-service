package org.com.clients;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.com.clients.fallback.LibraryClientFallback;
import org.com.entities.Users;
/*
 * Feign client Implementation of UserService
 * 
 * @author NikitaGayatri Kandukuri
 */
@Component
@FeignClient(name = "userservicerest",fallback=LibraryClientFallback.class)
public interface UserServiceClient {
	@GetMapping("users")
	public ResponseEntity<List<Users>> getAllUsers();

	@GetMapping("users/{user_id}")
	public ResponseEntity<Users> getuserById(@PathVariable long user_id);

	@PostMapping(value = "users")
	public ResponseEntity<Users> addUser(@RequestBody Users user);

	@DeleteMapping("users/{user_id}")
	public ResponseEntity<?> deleteUser(@PathVariable long user_id);

	@PutMapping("users/{user_id}")
	public ResponseEntity<Users> updateUser(@PathVariable long user_id, @RequestBody Users user);

}
