package org.com.clients.fallback;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.com.clients.BookServiceClient;
import org.com.entities.Book;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class BookClientFallback implements BookServiceClient {

	@Override
	public ResponseEntity<List<Book>> getAllBooks() {
		return ResponseEntity.of(Optional.of(Arrays.asList(Book.builder().id(1234).author("dummy").bookName("Dummy")
				.description("Not availble").genre("Not available").build())));
	}

	@Override
	public ResponseEntity<Book> getBookById(int id) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		return ResponseEntity.of(Optional.of((Book.builder().id(1234).author("dummy").bookName("Dummy")
//				.description("Not availble").genre("Not available").build())));
	}

	@Override
	public ResponseEntity<Book> addBook(Book book) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<?> deleteBook(int id) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<Book> updateBook(int id, Book book) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

}
