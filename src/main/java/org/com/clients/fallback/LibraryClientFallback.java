package org.com.clients.fallback;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.com.clients.UserServiceClient;
import org.com.entities.Users;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class LibraryClientFallback implements UserServiceClient{

	@Override
	public ResponseEntity<List<Users>> getAllUsers() {
		return ResponseEntity.of(Optional.of(Arrays.asList(Users.builder()
				.userId(1234)
				.userName("dummyuser")
				.emailId("Dummy")
				.mobile("Not availble")
				.build())));
	}

	@Override
	public ResponseEntity<Users> getuserById(long user_id) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<Users> addUser(Users user) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<?> deleteUser(long user_id) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<Users> updateUser(long user_id, Users user) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

}
