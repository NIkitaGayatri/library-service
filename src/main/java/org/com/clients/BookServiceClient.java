package org.com.clients;

import java.util.List;

import org.com.clients.fallback.BookClientFallback;
import org.com.entities.Book;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
/*
 * Feign client Implementation of BookService
 * 
 * @author NikitaGayatri Kandukuri
 */
@Component
@FeignClient(name = "booksservicerest",fallback=BookClientFallback.class)
public interface BookServiceClient {
	@GetMapping("books")
	ResponseEntity<List<Book>> getAllBooks();

	@GetMapping("book/{id}")
	ResponseEntity<Book> getBookById(@PathVariable int id);

	@PostMapping(value = "/book")
	ResponseEntity<Book> addBook(@RequestBody Book book);

	@DeleteMapping("book/{id}")
	 ResponseEntity<?> deleteBook(@PathVariable int id);

	@PutMapping("book/{id}")
	 ResponseEntity<Book> updateBook(@PathVariable int id, @RequestBody Book book);

}
