package org.com.entities;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Users {
	@ApiModelProperty(notes = "Specific Id of the user")
	long userId;
	@ApiModelProperty(notes = "Name of the user")
	String userName;
	@ApiModelProperty(notes = "EmailId of the user")
	String emailId;
	String mobile;
}
