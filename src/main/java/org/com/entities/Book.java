package org.com.entities;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Book {
	@ApiModelProperty(notes = "Specific id corresponding to the book")
	private int id;

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGenre() {
		return genre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ApiModelProperty(notes = "The book name of a book")
	private String bookName;
	@ApiModelProperty(notes = "The author of a book")
	private String author;
	@ApiModelProperty(notes = "Type of a book")
	private String genre;
	@ApiModelProperty(notes = "Desription about book")
	private String description;

    public void setGenre(String sample) {
    	this.genre=sample;
    }
}
