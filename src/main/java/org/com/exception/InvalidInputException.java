package org.com.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class InvalidInputException extends RuntimeException{
	private static final Logger log = LogManager.getLogger(InvalidInputException.class);

	public InvalidInputException(String message) {
		super(message);
		log.info(message);
	}
}
