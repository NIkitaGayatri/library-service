package org.com.exception;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
/*
 * class containing Handlers for Exception
 * 
 * @author NikitaGayatri Kandukuri
 */
@RestControllerAdvice
public class ExceptionController {
	private static final Logger logger=LogManager.getLogger("ExceptionController.class");
	/*
	 * Handling UserNotFoundException.This exception is thrown
	 * when user enters a Id which is not in the system
	 * 
	 * @return ResponseEntity with status code 404
	 */
	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<Object> exceptionHandler(final HttpServletRequest request,Exception exception) {
		logger.error(request.getRequestURI());
		ExceptionResponse exceptionResponse=new ExceptionResponse(exception.getMessage(),request.getRequestURI());
		return new ResponseEntity<>(exceptionResponse,HttpStatus.NOT_FOUND);
	}
	/*
	 * Handling BookNotFoundException.This exception is thrown
	 * when user enters a bookId which is not in the system
	 * 
	 * @return ResponseEntity with status code 404
	 */
	@ExceptionHandler(BookNotFoundException.class)
	public ResponseEntity<Object> bookExceptionHandler(final HttpServletRequest request,BookNotFoundException exception) {
		logger.error(request.getRequestURI());
		ExceptionResponse exceptionResponse=new ExceptionResponse(exception.getMessage(),request.getRequestURI());
		return new ResponseEntity<>(exceptionResponse,HttpStatus.NOT_FOUND);
	}
	/*
	 * Handling BookNotFoundException.This exception is thrown
	 * when user enters a bookId  or userId which is null
	 * which is not in the system
	 * 
	 * @return ResponseEntity with status code 404
	 */
	@ExceptionHandler(InvalidInputException.class)
	public ResponseEntity<Object> invalidInputExceptionHandler(final HttpServletRequest request,InvalidInputException exception) {
		logger.error(request.getRequestURI());
		ExceptionResponse exceptionResponse=new ExceptionResponse(exception.getMessage(),request.getRequestURI());
		return new ResponseEntity<>(exceptionResponse,HttpStatus.NOT_FOUND);
	}
}
