package org.com.repository;

import java.util.List;

import org.com.entities.Library;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface LibraryRepository extends JpaRepository<Library,Long>{

	@Transactional
	@Modifying
	@Query(value="DELETE from library where user_id=?1 and book_id=?2",nativeQuery = true)
	public void deleteByUserId(long user_id, int book_id);
	@Query(value="SELECT * from library where user_id=?1",nativeQuery = true)
	public List<Library> findByUserId(long userId);
	@Transactional
	@Modifying
	@Query(value="DELETE from library where user_id=?1",nativeQuery = true)
	public void deleteBooksByUserId(long userId);

	
}
