package org.com.service;

import java.util.List;

import org.com.clients.BookServiceClient;
import org.com.entities.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
@Service
public class BookService {
	@Autowired
	BookServiceClient bookServiceClient;
	/**
	 * This method is used to return list of books.
	 * 
	 * @return List<Book> This returns List of all Books.
	 */
	public ResponseEntity<List<Book>> getAllBooks() {
		return bookServiceClient.getAllBooks();
	}
	/**
	 * This method is used to return book of particular Id
	 * 
	 * @param id This is the first parameter to getBookById method
	 * @return Book This returns a Book of that particular Id.
	 */
	public ResponseEntity<Book> getBookById(int id) {
		return bookServiceClient.getBookById(id);
	}
	/**
	 * This method is used to add a new book
	 * 
	 * @param Book This is the first parameter to createBook method
	 */

	public void createBook(Book book) {
		bookServiceClient.addBook(book);
	}
	/**
	 * This method is used to delete a particular book
	 * 
	 * @param id This is the first parameter to deleteBook method
	 * to delete particular id
	 */

	public void deleteBook(int id) {
		bookServiceClient.deleteBook(id);
	}
	/**
	 * This method is used to Update a book
	 * 
	 * @param id   is the first parameter to specify the book Id to update
	 * @param user This is the second parameter to updateBook method
	 */
	public void updateBook(int id, Book book) {
		bookServiceClient.updateBook(id, book);
	}

}
