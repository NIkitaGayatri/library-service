package org.com.service;

import java.util.ArrayList;
import java.util.List;

import org.com.entities.Book;
import org.com.entities.Library;
import org.com.exception.BookNotFoundException;
import org.com.repository.LibraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class LibraryService {
	@Autowired
	LibraryRepository libraryRepository;
	@Autowired
	BookService bookService;
	/*
	 * This method is used to issue book to the user
	 * 
	 * @param userId specifies user Id
	 * 
	 * @param bookId specifies the n=book the user needs
	 */

	public void issueBookToUser(long user_id, int book_id) {
		Library library = new Library();
		library.setUser_id(user_id);
		library.setBook_id(book_id);
		 libraryRepository.save(library);
	}
	public List<Book>booksCorrespondingToUser(long UserId){
		List<Library>libraryList= libraryRepository.findByUserId(UserId);
		if(libraryList.isEmpty()) {
			throw new BookNotFoundException("No books available corresponding to user "+UserId);
		}
		List<Book>bookList=new ArrayList<>();
		for(Library library:libraryList) {
			ResponseEntity<Book> bookResponse = bookService.getBookById(library.getBook_id());
			bookList.add(bookResponse.getBody());
		}
		return bookList;	
	}
	public void deleteBooksCorrespondingToUser(long userId) {
		libraryRepository.deleteBooksByUserId(userId);
	}
	/*
	 * This method is used to delete book to the user
	 * 
	 * @param userId specifies user Id
	 * 
	 * @param bookId specifies the n=book the user needs
	 */
	public void deleteEntryInLibrary(long user_id, int book_id) {
		  libraryRepository.deleteByUserId(user_id, book_id);

	}

}
