package org.com.service;

import java.util.List;

import org.com.clients.UserServiceClient;
import org.com.entities.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
/**
 * This class is used to perform all UserService Operations like getting all
 * users,finding user By Id,adding User,deleting User Updating User
 * 
 * @author NikitaGayatri
 */
@Service
public class UserService {
	@Autowired
	UserServiceClient userServiceClient;
	@Autowired
	LibraryService libraryService;
	/**
	 * This method is used to return list of users.
	 * 
	 * @return List<Users> This returns List of all Users.
	 */
	public ResponseEntity<List<Users>> getAllUsers() {
		return userServiceClient.getAllUsers();
	}

	/**
	 * This method is used to return user of particular userId
	 * 
	 * @param id This is the first parameter to getUserById method
	 * @return User This returns a User of that particular Id.
	 */

	public ResponseEntity<Users> getUserById(long userId) {
		return userServiceClient.getuserById(userId);
	}
	/**
	 * This method is used to add a new user
	 * 
	 * @param user This is the first parameter to addUser method
	 */

	public void addUser(Users user) {
		userServiceClient.addUser(user);

	}
	/**
	 * This method is used to Update a user
	 * 
	 * @param id   is the first parameter to specify the user Id to update
	 * @param user This is the second parameter to updateUser method
	 */

	public void updateUser(long id, Users updateUser) {
		userServiceClient.updateUser(id, updateUser);

	}
	/**
	 * This method is used to delete a particular user
	 * 
	 * @param id This is the first parameter to deleteUser method
	 */
	public void deleteUser(long user_id) {
		userServiceClient.deleteUser(user_id);
	}

}
