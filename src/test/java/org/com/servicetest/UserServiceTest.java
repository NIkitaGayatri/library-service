package org.com.servicetest;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.com.clients.UserServiceClient;
import org.com.entities.Users;
import org.com.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class UserServiceTest {

	@InjectMocks
	UserService userService;
	@Mock
	UserServiceClient userServiceClient;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	@Test
	public void getAllUsersTest() {
		List<Users> usersList = new ArrayList<>();
		usersList.add(new Users(1,"Gayatri","gayatri@gmail.com","129301"));
		usersList.add(new Users(2,"Vyshu","vyshu@gmail.com","129301"));
	     ResponseEntity<List<Users>> expectedResponse=new ResponseEntity<List<Users>>(usersList,HttpStatus.OK);
        when(userServiceClient.getAllUsers()).thenReturn(expectedResponse);
        ResponseEntity<List<Users>> actualResponse = userService.getAllUsers();
        assertEquals(actualResponse,expectedResponse);
	}
	@Test
	public void getUserByIdTest() {
		Users user=new Users(1,"gayatri","gayatri@gmail.com","1234");
		ResponseEntity<Users>expectedResponse=new ResponseEntity<Users>(user,HttpStatus.OK);
		when(userServiceClient.getuserById(Mockito.anyLong())).thenReturn(expectedResponse);
		ResponseEntity<Users>actualResponse=userService.getUserById(Mockito.anyLong());
		assertEquals(actualResponse,expectedResponse);
	}
	
	@Test
	public void addUserTest() {
		userService.addUser(Mockito.any());
		verify(userServiceClient, times(1)).addUser(Mockito.any());
	}
	
	@Test
	public void updateUserTest() {
		userService.updateUser(Mockito.anyLong(), Mockito.any());
		verify(userServiceClient, times(1)).updateUser(Mockito.anyLong(),Mockito.any());
	}
	@Test
	public void deleteUserTest(){
		userService.deleteUser(Mockito.anyLong());
		verify(userServiceClient, times(1)).deleteUser(Mockito.anyLong());
	}

}
