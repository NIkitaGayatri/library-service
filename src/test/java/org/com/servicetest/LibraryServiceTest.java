package org.com.servicetest;

import static org.mockito.Mockito.when;


import org.com.entities.Library;
import org.com.repository.LibraryRepository;
import org.com.service.LibraryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class LibraryServiceTest {
	@InjectMocks
	LibraryService libraryService;
	@Mock
	LibraryRepository libraryRepository;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
//	@Test
//	public void issueBookToUserTest() {
//		Library library=new Library();
//		library.setUser_id(1);
//		library.setBook_id(1);
//		when(libraryRepository.save(Mockito.any())).thenReturn(library);
//		libraryService.issueBookToUser(Mockito.anyLong(),Mockito.anyInt());
//		
//	}
	@Test
	public void deleteEntryInLibrary()  {
		Library library=new Library(1,1,1);
		//when(libraryRepository.deleteByUserId(Mockito.anyLong(),Mockito.anyInt())).thenReturn(library);
		libraryService.deleteEntryInLibrary(Mockito.anyLong(),Mockito.anyInt());
	}
}
