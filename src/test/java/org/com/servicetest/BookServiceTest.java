package org.com.servicetest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.com.clients.BookServiceClient;
import org.com.entities.Book;
import org.com.exception.BookNotFoundException;
import org.com.service.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class BookServiceTest {
	@InjectMocks
	BookService bookService;
	@Mock
	BookServiceClient bookServiceClient;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getAllBooksTest() {
		List<Book> booksList = new ArrayList<>();
		booksList.add(new Book(1, "J.k.rowling", "Harrypoter", "fantasy",
				"Book contains information about harrypoter magic tricks"));
		booksList
				.add(new Book(2, "ramanujan", "maths", "logical", "Book contains solutions for various math problems"));
		ResponseEntity<List<Book>> expectedResponse = new ResponseEntity<List<Book>>(booksList, HttpStatus.OK);
		when(bookServiceClient.getAllBooks()).thenReturn(expectedResponse);
		ResponseEntity<List<Book>> actualResponse = bookService.getAllBooks();
		assertEquals(actualResponse, expectedResponse);
	}

	@Test
	public void getBookIdTest() throws BookNotFoundException {
		Book book = new Book(1, "J.k.rowling", "Harrypoter", "fantasy",
				"Book contains information about harrypoter magic tricks");
		ResponseEntity<Book> expectedResponse = new ResponseEntity<Book>(book, HttpStatus.OK);
		when(bookServiceClient.getBookById(Mockito.anyInt())).thenReturn(expectedResponse);
		ResponseEntity<Book> actualResponse = bookService.getBookById(Mockito.anyInt());
		assertEquals(actualResponse, expectedResponse);
	}

	@Test
	public void createBookTest() {
		bookService.createBook(Mockito.any());
		verify(bookServiceClient, times(1)).addBook(Mockito.any());
	}

	@Test
	public void deleteBookTest() {
		bookService.deleteBook(Mockito.anyInt());
		verify(bookServiceClient, times(1)).deleteBook(Mockito.anyInt());
	}

	@Test
	public void updateBookTest() throws BookNotFoundException {
		bookService.updateBook(Mockito.anyInt(), Mockito.any());
		verify(bookServiceClient, times(1)).updateBook(Mockito.anyInt(), Mockito.any());
	}

}
