package org.com.repositorytest;

import static org.assertj.core.api.Assertions.assertThat;

import org.com.entities.Library;
import org.com.repository.LibraryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
 public class LibraryRepositoryTest {
	@Autowired
	LibraryRepository libraryRespository;

	@Test
	public void testSaveBook() {
		Library library=new Library();
		library.setUser_id(1L);
		library.setBook_id(1);	
		libraryRespository.save(library);
		assertThat(library.getBook_id()).isNotNull();
	}


}
