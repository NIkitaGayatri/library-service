package org.com.pactTestForBooks;

import java.util.HashMap;
import java.util.Map;

import org.com.entities.Book;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;

@TestComponent
public class DeletePactTest {
	@Rule
	public PactProviderRuleMk2 mockProvider = new PactProviderRuleMk2("book-service", "10.71.12.198", 1000, this);

		@Pact(provider="book-service",consumer="library-service")
	    public RequestResponsePact createPactForGet(PactDslWithProvider builder) {
	        Map<String,String> headers=new HashMap<>();
	        headers.put("Content-Type","application/json");
	        
	        return builder
	                .uponReceiving("DELETE REQUEST")
	                .path("/book/15")
	                .method("DELETE")
	                .willRespondWith()
	                .status(204)
	                .toPact();
	        
	    }
		@Test
		@PactVerification("book-service")
		public void testDeleteCallForBookService() {
			RestTemplate restTemplate = new RestTemplate();
			Book expectedResponse = new Book();
			expectedResponse.setId(15);
			expectedResponse.setAuthor("valmiki");
			expectedResponse.setDescription("story about god rama");
			expectedResponse.setBookName("ramayana");
			expectedResponse.setGenre("ithihasa");
			HttpEntity<Book>request=new HttpEntity<>(expectedResponse);
			UriComponents builder=UriComponentsBuilder.fromHttpUrl(mockProvider.getConfig().url()+"/book/15").build();
			ResponseEntity<Book>response=restTemplate.exchange(builder.toUri(),HttpMethod.DELETE,request,Book.class);
			Assert.assertEquals(204, response.getStatusCodeValue());		
			}

}
