package org.com.pactTestForBooks;

import java.util.HashMap;
import java.util.Map;

import dispatch.Http;
import org.junit.Rule;
import org.junit.Test;
import org.com.entities.Book;
import org.junit.Assert;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@TestComponent
public class PostPactTest {
	@Rule
	public PactProviderRuleMk2 mockProvider = new PactProviderRuleMk2("book-service", "localhost", 1000, this);

	@Pact(provider = "book-service", consumer = "library-service")
	public RequestResponsePact createPactForPost(PactDslWithProvider builder) {
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");

		return builder
				.uponReceiving("POST REQUEST")
				.path("/book")
				.method("POST")
				.headers(headers)
				.body("{"+"\"id\":17,"+"\"bookName\":\"ramayana\","+
						"\"genre\":\"ithihasa\","+
						"\"author\":\"valmiki\","+
						"\"description\":\"story about god rama\""+"}")
				.path("/book")
				.willRespondWith()
				.headers(headers)
				.status(201)
				.body("{"+"\"id\":17,"+"\"bookName\":\"ramayana\","+
						"\"genre\":\"ithihasa\","+
						"\"author\":\"valmiki\","+
						"\"description\":\"story about god rama\""+"}")
				.toPact();

	}

	@Test
	@PactVerification("book-service")
	public void testPostMethodForBookService() {
		RestTemplate restTemplate = new RestTemplate();
		Book expectedResponse = new Book();
		expectedResponse.setId(17);
		expectedResponse.setBookName("ramayana");
		expectedResponse.setGenre("ithihasa");
		expectedResponse.setAuthor("valmiki");
		expectedResponse.setDescription("story about god rama");

		final Book response=restTemplate.postForObject(mockProvider.getConfig().url()+"/book",expectedResponse,Book.class);
		Assert.assertEquals(expectedResponse.getBookName(), response.getBookName());

	}

}
