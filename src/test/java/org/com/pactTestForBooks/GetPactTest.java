package org.com.pactTestForBooks;

import java.util.HashMap;
import java.util.Map;

import org.com.entities.Book;

import org.junit.Rule;
import org.junit.Test;
import org.junit.Assert;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.web.client.RestTemplate;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslResponse;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;

@TestComponent
public class GetPactTest {

	@Rule
	public PactProviderRuleMk2 mockProvider = new PactProviderRuleMk2("book-service", "10.71.12.198", 1000, this);

		@Pact(provider="book-service",consumer="library-service")
	    public RequestResponsePact createPactForGet(PactDslWithProvider builder) {
	        Map<String,String> headers=new HashMap<>();
	        headers.put("Content-Type","application/json");
	        
	        return builder
	                .uponReceiving("GET REQUEST")
	                .path("/book/1")
	                .method("GET")
	                .willRespondWith()
	                .status(200)
	                .headers(headers)
	                .body("{"+"\"id\":1,"+"\"bookName\":\"ramayana\","+
	                        "\"genre\":\"ithihasa\","+
	                        "\"author\":\"valmiki\","+
	                        "\"description\":\"story about god rama\""+"}")
	                .toPact();
	        
	    }

	@Test
	@PactVerification("book-service")
	public void testGetCallForBookService() {
		RestTemplate restTemplate = new RestTemplate();
		Book expectedResponse = new Book();
		expectedResponse.setId(1);
		expectedResponse.setAuthor("valmiki");
		expectedResponse.setDescription("story about god rama");
		expectedResponse.setBookName("ramayana");
		expectedResponse.setGenre("ithihasa");
		final  Book response = restTemplate.getForObject(mockProvider.getConfig().url() + "/book/1", Book.class);
		Assert.assertEquals(expectedResponse.getId(), response.getId());
	}

}
