package org.com.pactTestForBooks;

import java.util.HashMap;
import java.util.Map;

import org.com.entities.Book;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;

@TestComponent
public class PutPactTest {
	@Rule
	public PactProviderRuleMk2 mockProvider = new PactProviderRuleMk2("book-service", "localhost", 1000, this);

	@Pact(provider = "book-service", consumer = "library-service")
	public RequestResponsePact createPactForPut(PactDslWithProvider builder) {
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");

		return builder
				.uponReceiving("PUT REQUEST")
				.path("/book/11")
				.method("PUT")
				.headers(headers)
				.body("{" + "\"id\":11," + "\"bookName\":\"ramayana\"," + "\"genre\":\"ithihasa\","
						+ "\"author\":\"valmiki\"," + "\"description\":\"nothing\"" + "}")
				.willRespondWith()
				.status(201)
				.toPact();

	}

	@Test
	@PactVerification("book-service")
	public void testPutCallForBookService() {
		RestTemplate restTemplate = new RestTemplate();
		Book expectedResponse = new Book();
		expectedResponse.setId(11);
		expectedResponse.setAuthor("valmiki");
		expectedResponse.setDescription("nothing");
		expectedResponse.setBookName("ramayana");
		expectedResponse.setGenre("ithihasa");
		HttpEntity<Book> request = new HttpEntity<>(expectedResponse);
		UriComponents builder = UriComponentsBuilder.fromHttpUrl("http://localhost:1000/book/11").build();
		ResponseEntity<Book> response = restTemplate.exchange(builder.toUri(), HttpMethod.PUT, request, Book.class);
		Assert.assertEquals(201, response.getStatusCodeValue());

	}
}
